﻿$(document).ready(function () {
    var currentAudioIndex = 0;
    var musicplayer = document.getElementById('music');
    var musicSrc = document.getElementById('musicSrc');
    var playAndPauseBtn = document.getElementById('playAndPauseBtn');
    var durationFormattedTime;
    var activePlaylist = 0;
    var visiblePlaylist = 0;
    var currentPlaylists = [];
    var currentlyPlayingSong;

    function playlist(id, name, index, list, listSize) {
        this.plId = id;
        this.plName = name;
        this.playPos = index;
        this.previousPlayPos = 0;
        this.musicList = list;
        this.nrOfSongs = listSize;
    }
    initMusic();

    function initMusic() {
        currentPlaylists[0] = new playlist(0, "Library View", 0, [], 0);
        getAllPlaylists();
        updatePlaylistDropdown();
        setPlaylistSizes();
    }

    function setPlaylistSizes() {
        for (var p = 0; p < currentPlaylists.length; p++) {
            currentPlaylists[p].nrOfSongs = 0;
            for (var a = 0; a < currentPlaylists[p].musicList.length; a++) {
                currentPlaylists[p].nrOfSongs += currentPlaylists[p].musicList[a].musicfiles.length;
            }
        }
    }

    function updatePlaylistSize(id) {
        currentPlaylists[id].nrOfSongs = 0;
        for (var a = 0; a < currentPlaylists[id].musicList.length; a++) {
            currentPlaylists[id].nrOfSongs += currentPlaylists[id].musicList[a].musicfiles.length;
        }
    }

    function getAllPlaylists() {
        $.ajax({
            url: "Music/GetPlaylists",
            type: 'POST',
            success: function (playlists) {
                if (playlists.length > 0) {
                    for (var i = 0; i < playlists.length; i++) {
                        currentPlaylists[i + 1] = new playlist(playlist[i].id, playlists[i].name, 0, []);
                        alert("currentplaylist[" + i + 1 + "].name = " + currentPlaylists[i + 1].plName);
                    }
                }
            },
            error: function () {
                alert("something seems wrong");
            }
        });
    }

    function updatePlaylistDropdown() {
        var htmlcontent = "";
        for (var i = 0; i < currentPlaylists.length; i++) {
            htmlcontent = "<option value='" + i + "'>" + currentPlaylists[i].plName + "</option>";
        }
        $("#playlistDropDown").html(htmlcontent);
    }

    function populateMusicPlaylistByFolderId(id) {
        $.ajax({
            url: "Music/GetAlbumsByFolderId",
            type: 'POST',
            data: {
                id: id
            },
            success: function (musicData) {
                currentPlaylists[activePlaylist].musicList = musicData;
                currentPlaylists[activePlaylist].playPos = 0;
                currentPlaylists[activePlaylist].previousPlayPos = 0;
                updatePlaylistSize(activePlaylist);
                updatePlayList(currentPlaylists[activePlaylist].musicList)
            },
            error: function () {
                alert("something seems wrong");
            }
        });
    }

    function updatePlayList(musicFiles) {
        var htmlContent = "<thead><tr><th id='isPlaying' class='columnIcon'></th><th id='title'>Title</th><th id='artist'>Artist</th><th id='album'>Album</th><th id='duration'>Duration</th></tr></thead>";
        htmlContent += "<tbody>";
        var rowCount = 0;
        for (var a = 0; a < musicFiles.length; a++) {
            var currentAlbum = musicFiles[a];
            for (var m = 0; m < currentAlbum.musicfiles.length; m++) {
                htmlContent += "<tr>";
                htmlContent += "<td id='playlistFirstColumnRow" + rowCount + "' class='playListFirstColumn'></td><td>" + currentAlbum.musicfiles[m].title + "</td><td>" + currentAlbum.musicfiles[m].artist.name + "</td><td>" + currentAlbum.name + "</td><td>" + currentAlbum.musicfiles[m].lenght + "</td>";
                htmlContent += "</tr>";
                rowCount++;
            }
        }
        htmlContent += "</tbody>";
        $("#playlistTable").html(htmlContent);
        for (var i = 1; i < playlistTable.getElementsByTagName("tr").length; i++) {
            var row = playlistTable.getElementsByTagName("tr")[i];
            row.ondblclick = function () {
                if (activePlaylist != visiblePlaylist) {
                    activePlaylist = visiblePlaylist;
                }
                changeSongInActivePl(this.rowIndex - 1, 0)
            };
        }
    }
    $('#jstree').jstree({
        'core': {
            'data': {
                'url': '/Music/PopulateJsTreeWithUserMusic',
                'type': 'POST',
                'data': function (node) {
                    return {
                        'id': node.id,
                        'parent': node.parent
                    };
                }
            },
            "themes": {
                "name": "default-dark",
                "dots": true,
                "icons": true
            },
            "search": {
                "case_insensitive": true,
                "show_only_matches": true
            }
        },
        "plugins": ["contextmenu", "dnd", "search"]
    });
    $("#jsTreeFilterInput").keyup(function () {
        var searchString = $(this).val();
        console.log(searchString);
        $('#jstree').jstree('search', searchString);
    });
    $('#jstree').on("select_node.jstree", function (e, data) {
        if (data.node.id.substring(0, 1).localeCompare("0")) {
            populateMusicPlaylistByFolderId(data.node.id);
        }
    });
    $(function () {
        $("#slider").slider({
            orientation: "horizontal",
            min: 0,
            max: 100,
            range: "min",
            value: 0,
            slide: function (event, ui) {
                musicplayer.currentTime = (ui.value / 100) * musicplayer.duration;
            }
        });
    });
    var responsiveBreakPointFirst = 767;
    $(function () {
        $("#slider-vertical").slider({
            orientation: "horizontal",
            min: 0,
            max: 100,
            range: "min",
            value: 50,
            slide: function (event, ui) {
                musicplayer.volume = ui.value / 100;
                if (musicplayer.muted) musicplayer.muted = false;
                switchToAudioPic();
            }
        });
        musicplayer.volume = $("#slider-vertical").slider("value") / 100;
        if ($(window).width() <= responsiveBreakPointFirst) {
            $("#muteBtn").unbind("click");
            $("#muteBtn").click(function () {
                $("#slider-vertical-container").toggle();
            });
            $("#slider-vertical").slider("option", "orientation", "vertical");
            $("#slider-vertical").slider("option", "range", false);
            $("#slider-vertical").slider("option", "range", "min");
        } else if ($(window).width() > responsiveBreakPointFirst) {
            $("#slider-vertical-container").removeAttr("style");
            $("#muteBtn").unbind("click");
            $("#muteBtn").click(function () {
                toggleSoundOnOf();
            });
        }
    });
    var underLimitFirst = false;
    $(window).resize(function () {
        if ($(window).width() <= responsiveBreakPointFirst && underLimitFirst == false) {
            underLimitFirst = true;
            $("#muteBtn").unbind("click");
            $("#muteBtn").click(function () {
                $("#slider-vertical-container").toggle();
            });
            $("#slider-vertical").slider("option", "orientation", "vertical");
            $("#slider-vertical").slider("option", "range", false);
            $("#slider-vertical").slider("option", "range", "min");
        } else if ($(window).width() > responsiveBreakPointFirst && underLimitFirst == true) {
            underLimitFirst = false;
            $("#slider-vertical-container").removeAttr("style");
            $("#muteBtn").unbind("click");
            $("#muteBtn").click(function () {
                toggleSoundOnOf();
            });
            $("#slider-vertical").slider("option", "orientation", "horizontal");
            $("#slider-vertical").slider("option", "range", false);
            $("#slider-vertical").slider("option", "range", "min");
        }
        var attr = $("#slider-container").attr('style');
        if (typeof attr !== typeof undefined && attr !== false && $(window).width() > 400) {
            $("#slider-container[style]").removeAttr("style");
        }
    });
    $("#timelineBtn").click(function () {
        $("#slider-container").toggle();
    });
    $("#muteBtn-vertical").click(function () {
        toggleSoundOnOf();
    });

    function toggleSoundOnOf() {
        if (musicplayer.muted) {
            switchToAudioPic();
        } else {
            switchToMutedPic();
        }
    }

    function switchToPlayPic() {
        $('span:first', playAndPauseBtn).prop('class', '');
        $('span:first', playAndPauseBtn).prop('class', 'glyphicon glyphicon-play');
    }

    function switchToPausePic() {
        $('span:first', playAndPauseBtn).prop('class', '');
        $('span:first', playAndPauseBtn).prop('class', 'glyphicon glyphicon-pause');
    }

    function switchToMutedPic() {
        $('span:first', "#muteBtn").prop('class', '');
        $('span:first', "#muteBtn").prop('class', 'glyphicon glyphicon-volume-off');
        $('span:first', "#muteBtn-vertical").prop('class', '');
        $('span:first', "#muteBtn-vertical").prop('class', 'glyphicon glyphicon-volume-off');
        musicplayer.muted = true;
    }

    function switchToAudioPic() {
        $('span:first', "#muteBtn").prop('class', '');
        $('span:first', "#muteBtn-vertical").prop('class', '');
        $('span:first', "#muteBtn-vertical").prop('class', 'glyphicon glyphicon-volume-up');
        if (musicplayer.volume <= 0.5) {
            $('span:first', "#muteBtn").prop('class', 'glyphicon glyphicon-volume-down');
        } else {
            $('span:first', "#muteBtn").prop('class', 'glyphicon glyphicon-volume-up');
        }
        musicplayer.muted = false;
    }

    function stopSong() {
        musicplayer.pause();
        musicplayer.currentTime = 0;
        switchToPlayPic();
        removeCurrentlyPlaying();
        $("#currentlyPlayingInfo span:first-of-type").html("Playback stopped.");
        document.getElementById("playingSongAndTimeSeperator").style.display = "none";
        document.getElementById("timeDisplayCurrentlyPlayingInfo").style.display = "none";
    }

    function changeSongInActivePl(index, direction) {
        var activePl = currentPlaylists[activePlaylist];
        var indexToPlay = 0;
        if ((activePl.playPos == 0 && index == -1 && direction == -1) || (activePl.playPos == activePl.nrOfSongs - 1 && index == -1 && direction == 1)) stopSong();
        else {
            if (index > 0 && index <= activePl.nrOfSongs) {
                activePl.previousPlayPos = activePl.playPos;
                indexToPlay = index;
                activePl.playPos = index;
            } else if (index == -1) {
                activePl.previousPlayPos = activePl.playPos;
                indexToPlay = activePl.playPos + direction;
                activePl.playPos += direction;
            }
            for (var i = 0; i < activePl.musicList.length; i++) {
                if (activePl.musicList[i].musicfiles.length > indexToPlay) {
                    musicSrc.src = activePl.musicList[i].musicfiles[indexToPlay].location;
                    currentlyPlayingSong = activePl.musicList[i].musicfiles[indexToPlay];
                    musicplayer.load();
                    musicSrc.src = "";
                    musicplayer.play();
                    changeCurrentlyPlaying(activePl.previousPlayPos);
                    break;
                } else {
                    indexToPlay -= activePl.musicList[i].musicfiles.length;
                }
            }
        }
    }

    function removeAllClassesFromCurrenltyPlaying() {
        if ($('.playListFirstColumn').is('#currentlyPlaying')) {
            if ($("#currentlyPlaying").hasClass("glyphicon glyphicon-play")) {
                $("#currentlyPlaying").removeClass("glyphicon glyphicon-play");
            }
            if ($("#currentlyPlaying").hasClass("glyphicon glyphicon-pause")) $("#currentlyPlaying").removeClass("glyphicon glyphicon-pause");
        }
    }

    function removeCurrentlyPlaying() {
        if ($('.playListFirstColumn').is('#currentlyPlaying')) {
            removeAllClassesFromCurrenltyPlaying();
            $("#currentlyPlaying").attr("id", "playlistFirstColumnRow" + currentPlaylists[activePlaylist].playPos);
        }
    }

    function changeCurrentlyPlaying(previousAudioIndex) {
        if ($('.playListFirstColumn').is('#currentlyPlaying')) {
            removeAllClassesFromCurrenltyPlaying();
            $("#currentlyPlaying").attr("id", "playlistFirstColumnRow" + currentPlaylists[activePlaylist].previousPlayPos);
            $("#playlistFirstColumnRow" + currentPlaylists[activePlaylist].playPos).attr("id", "currentlyPlaying");
            $("#currentlyPlaying").addClass("glyphicon glyphicon-play");
        } else {
            $("#playlistFirstColumnRow" + currentPlaylists[activePlaylist].playPos).attr("id", "currentlyPlaying");
            $("#currentlyPlaying").addClass("glyphicon glyphicon-play");
        }
    }

    function secToFormattedTime(sec) {
        var formattedTime = "";
        var hours = 0,
			min = 0,
			secLeft = 0;
        sec = Math.floor(sec);
        if (sec >= 3600) {
            hours = ~~ (sec / 3600);
            sec -= 3600 * hours;
        }
        min = ~~ (sec / 60);
        sec -= 60 * min;
        secLeft = sec;
        if (hours != 0) formattedTime = hours + ":";
        if (hours != 0 && min < 10) formattedTime += "0" + min + ":";
        else formattedTime += min + ":"; if (secLeft < 10) formattedTime += "0" + secLeft;
        else formattedTime += secLeft;
        return formattedTime;
    }

    function timeUpdate() {
        var playPercent = 100 * (musicplayer.currentTime / musicplayer.duration);
        $("#slider").slider("option", "value", playPercent);
        var currentFormattedTime = secToFormattedTime(musicplayer.currentTime);
        $(".timeDisplay p:first-of-type").html(currentFormattedTime + " / " + durationFormattedTime);
        if (musicplayer.ended) {
            changeSongInActivePl(-1, 1);
        }
    }

    function playUpdate() {
        if ($('.playListFirstColumn').is('#currentlyPlaying')) {
            if (musicplayer.paused) {
                if ($("#currentlyPlaying").hasClass("glyphicon glyphicon-play")) $("#currentlyPlaying").removeClass("glyphicon glyphicon-play");
                $("#currentlyPlaying").addClass("glyphicon glyphicon-pause");
            } else {
                if ($("#currentlyPlaying").hasClass("glyphicon glyphicon-pause")) $("#currentlyPlaying").removeClass("glyphicon glyphicon-pause");
                $("#currentlyPlaying").addClass("glyphicon glyphicon-play");
            }
        } else {
            $("#playlistFirstColumnRow" + currentPlaylists[activePlaylist].playPos).attr("id", "currentlyPlaying");
            $("#currentlyPlaying").addClass("glyphicon glyphicon-play");
        }
    }
    musicplayer.addEventListener("timeupdate", timeUpdate, false);
    musicplayer.addEventListener("canplay", function () {
        durationFormattedTime = secToFormattedTime(musicplayer.duration);
    }, false);
    $("#stopBtn").click(function () {
        stopSong();
    });
    $(playAndPauseBtn).click(function () {
        if (musicplayer.paused) {
            musicplayer.play();
        } else {
            musicplayer.pause();
        }
        playUpdate();
    });
    musicplayer.onplay = function () {
        $("#currentlyPlayingInfo span:first-of-type").html(currentlyPlayingSong.artist.name + " - " + currentlyPlayingSong.album + " - " + currentlyPlayingSong.title);
        document.getElementById("playingSongAndTimeSeperator").style.display = "block";
        document.getElementById("timeDisplayCurrentlyPlayingInfo").style.display = "block";
        switchToPausePic();
    };
    musicplayer.onpause = function () {
        switchToPlayPic();
    };
    var libraryViewJustBeenUpdated = false;
    $("#backwardBtn").click(function () {
        changeSongInActivePl(-1, -1);
    });
    $("#forwardBtn").click(function () {
        changeSongInActivePl(-1, 1);
    });
});