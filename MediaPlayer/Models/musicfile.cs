//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MediaPlayer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class musicfile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public musicfile()
        {
            this.folder1 = new HashSet<folder>();
            this.artist = new HashSet<artist>();
            this.siteuser = new HashSet<siteuser>();
            this.playlist = new HashSet<playlist>();
        }
    
        public int id { get; set; }
        public string filename { get; set; }
        public Nullable<int> folder_id { get; set; }
        public string audioformat { get; set; }
        public string lenght { get; set; }
        public string channels { get; set; }
        public string bitrate { get; set; }
        public string title { get; set; }
        public Nullable<int> album_id { get; set; }
        public string tracknr { get; set; }
        public Nullable<int> genre_id { get; set; }
        public string songyear { get; set; }
        public string composer { get; set; }
        public string performer { get; set; }
        public string author { get; set; }
    
        public virtual album album { get; set; }
        public virtual folder folder { get; set; }
        public virtual genre genre { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<folder> folder1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<artist> artist { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<siteuser> siteuser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<playlist> playlist { get; set; }
    }
}
