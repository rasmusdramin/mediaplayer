﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MediaPlayer.Models
{
    public class MusicFileSimple
    {
        public int id { get; set; }
        public string filename { get; set; }
        public string location { get; set; }
        public string audioformat { get; set; }
        public string lenght { get; set; }
        public string channels { get; set; }
        public string bitrate { get; set; }
        public string title { get; set; }
        public string tracknr { get; set; }
        public GenreSimple genre { get; set; }
        public ArtistSimple artist { get; set; }
        public string songyear { get; set; }
        public string composer { get; set; }
        public string performer { get; set; }
        public string author { get; set; }
        public string album { get; set; }
    }
}