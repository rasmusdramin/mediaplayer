﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MediaPlayer.Models
{
    public class PlayListSimple
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}