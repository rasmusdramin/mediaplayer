﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MediaPlayer.Models
{
    public class JsTreeItem
    {
        public string id { get; set; }
        public string parent { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public State state { get; set; }
        public bool children { get; set; } 
    }
}