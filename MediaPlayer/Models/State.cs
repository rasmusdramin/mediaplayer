﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MediaPlayer.Models
{
    public class State
    {
        public bool opened { get; set; }
        public bool disabled { get; set; }
        public bool selected { get; set; }
    }
}