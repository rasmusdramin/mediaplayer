﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.ObjectModel;

namespace MediaPlayer.Models
{
    public class AlbumSimple
    {
        public int id { get; set; }
        public string name { get; set; }
        public Collection<int> years { get; set; }
        public Collection<ArtistSimple> artists { get; set; }
        public Collection<MusicFileSimple> musicfiles { get; set; }
        public int size { get; set; }
    }
}