﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MediaPlayer.Models
{
    public class UserModel
    {
        [Required]
        [StringLength(50)]
        [Display(Name="Username ")]
        public string username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(50)]
        [Display(Name = "Password ")]
        public String password { get; set; }
    }
}