﻿angular.module("ngLayoutApp", []).controller("ngSideMenuController", ['$scope', function ($scope) {
    $scope.showOptionsSideMenuRight = false;
    $scope.toggleSettingsMenu = function () {
        $scope.showRightSettingsMenu = !$scope.showRightSettingsMenu;
    };
}]);