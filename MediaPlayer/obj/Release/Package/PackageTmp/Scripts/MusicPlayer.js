﻿$(document).ready(function () {
    var musicplayer = document.getElementById('music');
    var musicSrc = document.getElementById('musicSrc');
    var playAndPauseBtn = document.getElementById('playAndPauseBtn');
    //var duration;
    var durationFormattedTime;
    var activePlaylist = 0;
    var visiblePlaylist = 0;
    var currentPlaylists = [];
    var currentlyPlayingSong;
    function playlist(id, name, index, list, listSize, songPos, chunkNr)
    {
        this.plId = id;
        this.plName = name;
        this.playPos = index;
        this.previousPlayPos = 0;
        this.musicList = list;
        this.nrOfSongs = listSize;
        this.songToPlayIndex = songPos;
        this.chunkToLoad = chunkNr;
    }
    var playlistchunksize = 100;
    var currentFolderID = 0;

    $(function () {
        $('div.split-pane').splitPane();
    });

    //END SPLIT VIEW

    //PLAYLIST
    $(function () {
        $("#playlist").selectable({
            filter: 'tbody tr'
        });
    });
    //END PLAYLIST

    initMusic();

    function initMusic()
    {
        currentPlaylists[0] = new playlist(0, "Library View", 0, [], 0, 0, 1);
        updatePlaylistDropdown();
        getAllPlaylists();
    }

    function setPlaylistSizes()
    {
        for(var p = 0; p < currentPlaylists.length; p++)
        {
            currentPlaylists[p].nrOfSongs = 0;
            for(var a = 0; a < currentPlaylists[p].musicList.length; a++)
            {
                currentPlaylists[p].nrOfSongs += currentPlaylists[p].musicList[a].musicfiles.length;
            }
        }
    }

    function updatePlaylistSize(id)
    {
        currentPlaylists[id].nrOfSongs = 0;
        for (var a = 0; a < currentPlaylists[id].musicList.length; a++) {
            if (currentPlaylists[id].musicList[a])
                currentPlaylists[id].nrOfSongs += currentPlaylists[id].musicList[a].musicfiles.length;
        }
    }

    function getAllPlaylists()
    {
        $.ajax({
            url: "Music/GetPlaylists",
            type: 'POST',
            success: function (playlists) {
                if(playlists.length > 0)
                {
                    for (var i = 0; i < playlists.length; i++)
                    {
                        currentPlaylists[parseInt(i) + 1] = new playlist(playlists[i].id, playlists[i].name, 0, []);
                        //alert("currentplaylist[" + (parseInt(i) + 1) + "].name = " + currentPlaylists[parseInt(i) + 1].plName);
                    }
                    setPlaylistSizes();
                    updatePlaylistDropdown();
                }
            },
            error: function () {
                alert("something seems wrong");
            }
        });
    }

    function updatePlaylistDropdown()
    {
        var htmlcontent = "";
        for (var i = 0; i < currentPlaylists.length; i++)
        {
            //alert(currentPlaylists[i].plName);

            //alert("if (" + i + " == " + visiblePlaylist + ")");
            if (parseInt(i) == parseInt(visiblePlaylist))
                htmlcontent += "<option value='" + i + "' selected >" + currentPlaylists[i].plName + "</option>";
            else
                htmlcontent += "<option value='" + i +"'>" + currentPlaylists[i].plName + "</option>";
        }
        
        $("#playlistDropDown").html(htmlcontent);
    }

    $("#createPlaylistBtn").click(function () {
        var newPlaylistName = $("#playlistnameInput").val();
        //alert(newPlaylistName);

        $.ajax({
            url: "Music/CreatePlaylist",
            type: 'POST',
            data: { playlistName: newPlaylistName },
            success: function (id) {
                currentPlaylists[currentPlaylists.length] = new playlist(id, newPlaylistName, 0, []);
                visiblePlaylist = currentPlaylists.length - 1;
                updatePlaylistDropdown();
            },
            error: function () {
                alert("something seems wrong");
            }
        });
    });

    $("#playlistDropDown").change(function () {
        //alert("Handler for .change() called. Value: " + $("#playlistDropDown").val());
        visiblePlaylist = $("#playlistDropDown").val();
        updatePlayList(currentPlaylists[visiblePlaylist].musicList);
    });

    musicsourceajaxloadsongoing = [];
    var wasThereMoreToLoad = true;

    function populateMusicPlaylistByFolderId(id) {

        if (musicsourceajaxloadsongoing) {
            for (var i = 0; i < musicsourceajaxloadsongoing.length - 1; i++)
                musicsourceajaxloadsongoing.abort();

            musicsourceajaxloadsongoing = [];
            //if (musicsourceajaxloadsongoing.length > 0) {
            //    var tempload = musicsourceajaxloadsongoing[musicsourceajaxloadsongoing.length - 1];
            //    musicsourceajaxloadsongoing = [];
            //    musicsourceajaxloadsongoing[0] = tempload;
            //}
        }

        var tempLoad = $.ajax({
            url: "Music/GetAlbumsByFolderId",
            type: 'POST',
            data: {id: id, chunkSize: playlistchunksize, chunkNr: currentPlaylists[0].chunkToLoad},
            success: function (musicData) {

                //alert(currentPlaylists[activePlaylist].chunkToLoad);

                if (!musicData[musicData.length-1]) {
                    musicData.pop();

                    wasThereMoreToLoad = false;
                }
                else
                    wasThereMoreToLoad = true;

                if (currentPlaylists[activePlaylist].chunkToLoad == 1)
                {
                    currentPlaylists[activePlaylist].musicList = musicData;
                    currentPlaylists[activePlaylist].playPos = 0;
                    currentPlaylists[activePlaylist].previousPlayPos = 0;

                    updatePlayList(currentPlaylists[visiblePlaylist].musicList);
                }
                else
                {
                    appendPlaylist(musicData);

                    //currentPlaylists[activePlaylist].musicList.concat(musicData);
                    for (var i = 0; i < musicData.length; i++)
                    {
                        currentPlaylists[activePlaylist].musicList.push(musicData[i]);
                    }
                }

                currentPlaylists[activePlaylist].chunkToLoad++;
                updatePlaylistSize(activePlaylist);
                $("#playlist").removeClass("loading");
                $("#playlist").removeClass("loading2");
                hideLoadMoreSongsBtn();

            },
            error: function () {
                alert("something seems wrong");
                $("#playlist").removeClass("loading");
                $("#playlist").removeClass("loading2");
            }
        });

        musicsourceajaxloadsongoing.push(tempLoad);
    }

    $("#loadMoreSongsBtn").click(function () {
        //alert("playlist.scrollTop(): " + $("#playlist").scrollTop() + "\n" + "playlist.height(): " + $("#playlist").height() + "\n" + "playlistTable.height(): " + $("#playlistTable").height());
        hideLoadMoreSongsBtn();
        $("#playlist").addClass("loading2");

        if(currentFolderID != 0)
        {
            if(wasThereMoreToLoad)
                populateMusicPlaylistByFolderId(currentFolderID);
            else
                $("#playlist").removeClass("loading2");
        }
    });

    //Check if scrolled to bottom of playlist
    $("#playlist").scroll(function () {
        if ($("#playlist").scrollTop() + $("#playlist").height() == $("#playlistTable").height()) {

            if (!$("#playlist").is(".loading2") && wasThereMoreToLoad)
                showLoadMoreSongsBtn();
        }
    });

    function hideLoadMoreSongsBtn() {
        $("#loadMoreSongsBtn").prop('class', 'hidden');
    }

    function showLoadMoreSongsBtn() {
        $("#loadMoreSongsBtn").prop('class', '');
    }

    function updatePlayList(musicFiles) {
        //var htmlContent = "<colgroup><col span='1' id='firstCol'><col span='1' id='secondCol'><col span='1' id='thirdCol'><col span='1' id='fourthCol'><col span='1' id='fifthCol'></colgroup>";
        
        var htmlContent = "<thead><tr><th id='isPlaying' class='columnIcon'></th><th id='title'>Title</th><th id='duration'>Duration</th></tr></thead>";
        htmlContent += "<tbody>";

        var songCount = 0;

        var length = musicFiles.length;

        if (length > 10)
            length = 10;

        for (var a = 0; a < length; a++) {

            //alert(musicFiles.length);
            var currentAlbum = musicFiles[a];

            htmlContent += "<tr value='" + songCount + "' class='albumSeperator'>";

            //Artist
            if (currentAlbum != null)
            {
                var artist;
                if (currentAlbum.artists) {
                    if (currentAlbum.artists.length > 1)
                        artist = "Various Artists";
                    else
                        artist = "" + currentAlbum.artists[0].name;
                }
                else {
                    artist = "???";
                }


                htmlContent += "<td class='albumTD' colspan='3'>" + artist + " - [" + currentAlbum.years[0] + "] " + currentAlbum.name + "</td>";

                htmlContent += "</tr>";

                for (var m = 0; m < currentAlbum.musicfiles.length; m++) {
                    htmlContent += "<tr value='" + songCount + "'>";

                    htmlContent += "<td id='playlistFirstColumnRow" + songCount + "' class='playListFirstColumn'></td><td>" + currentAlbum.musicfiles[m].title + "</td><td id='durationTD'>" + currentAlbum.musicfiles[m].lenght + "</td>";

                    htmlContent += "</tr>";

                    songCount++;
                }
            }


            //var artist;
            //if (currentAlbum.artists) {
            //    if (currentAlbum.artists.length > 1)
            //        artist = "Various Artists";
            //    else
            //        artist = "" + currentAlbum.artists[0].name;
            //}
            //else {
            //    artist = "???";
            //}

            
            //htmlContent += "<td class='albumTD' colspan='3'>" + artist + " - [" + currentAlbum.years[0] + "] " + currentAlbum.name + "</td>";

            //htmlContent += "</tr>";

            //for (var m = 0; m < currentAlbum.musicfiles.length; m++)
            //{
            //    htmlContent += "<tr value='" + songCount + "'>";

            //    htmlContent += "<td id='playlistFirstColumnRow" + songCount + "' class='playListFirstColumn'></td><td>" + currentAlbum.musicfiles[m].title + "</td><td id='durationTD'>" + currentAlbum.musicfiles[m].lenght + "</td>";

            //    htmlContent += "</tr>";

            //    songCount++;
            //}
        }

        htmlContent += "</tbody>";

        $("#playlistTable").html(htmlContent);

        $("#playlistTable").on('dblclick', 'tr', function (e) {
            e.preventDefault();

            if (activePlaylist != visiblePlaylist)
            {
                activePlaylist = visiblePlaylist;
            }

            changeSongInActivePl($(this).attr('value'), 0);
            //var id = $(this).attr('value');
            //alert(id);
        });
    }

    function appendPlaylist(musicFiles) {
        var songCount = $('#playlistTable tr').length - 1;
        songCount -= currentPlaylists[activePlaylist].musicList.length;

        //alert(songCount);

        var htmlContent = "";

        for (var a = 0; a < musicFiles.length; a++) {

            var currentAlbum = musicFiles[a];

            htmlContent += "<tr value='" + songCount + "' class='albumSeperator'>";

            //Artist
            if (currentAlbum != null)
            {
                var artist;
                if (currentAlbum.artists) {
                    if (currentAlbum.artists.length > 1)
                        artist = "Various Artists";
                    else
                        artist = "" + currentAlbum.artists[0].name;
                }
                else {
                    artist = "???";
                }

                //ALBUM Info Row
                htmlContent += "<td class='albumTD' colspan='3'>" + artist + " - [" + currentAlbum.years[0] + "] " + currentAlbum.name + "</td>";

                htmlContent += "</tr>";

                //MUSICFILES in album
                for (var m = 0; m < currentAlbum.musicfiles.length; m++) {
                    htmlContent += "<tr value='" + songCount + "'>";

                    htmlContent += "<td id='playlistFirstColumnRow" + songCount + "' class='playListFirstColumn'></td><td>" + currentAlbum.musicfiles[m].title + "</td><td id='durationTD'>" + currentAlbum.musicfiles[m].lenght + "</td>";

                    htmlContent += "</tr>";

                    songCount++;
                }
            }

            //var artist;
            //if (currentAlbum.artists) {
            //    if (currentAlbum.artists.length > 1)
            //        artist = "Various Artists";
            //    else
            //        artist = "" + currentAlbum.artists[0].name;
            //}
            //else {
            //    artist = "???";
            //}

            ////ALBUM Info Row
            //htmlContent += "<td class='albumTD' colspan='3'>" + artist + " - [" + currentAlbum.years[0] + "] " + currentAlbum.name + "</td>";

            //htmlContent += "</tr>";

            ////MUSICFILES in album
            //for (var m = 0; m < currentAlbum.musicfiles.length; m++) {
            //    htmlContent += "<tr value='" + songCount + "'>";

            //    htmlContent += "<td id='playlistFirstColumnRow" + songCount + "' class='playListFirstColumn'></td><td>" + currentAlbum.musicfiles[m].title + "</td><td id='durationTD'>" + currentAlbum.musicfiles[m].lenght + "</td>";

            //    htmlContent += "</tr>";

            //    songCount++;
            //}
        }

        $('#playlistTable tr:last').after(htmlContent);

        //$("#playlistTable").on('dblclick', 'tr', function (e) {
        //    e.preventDefault();
        //    //alert($(this).attr('value'));
        //    changeSongInActivePl($(this).attr('value'), 0);
        //});
    }

    //JSTREE

    //Populate jstree
    $('#jstree').jstree({
        'core': {
            'data': {
                'url': function (node) {
                    return node.id === '#' ?
                      'Music/GetMusicRootFolder' :
                      'Music/GetSubFoldersOfFolder';
                },
                'type': 'POST',
                'data': function (node) {
                    return { 'id': node.id};
                }
            },
            "themes": {
                "name": "default-dark",
                "dots": true,
                "icons": true
            },
            "search": {
                "case_insensitive": true,
                "show_only_matches" : true
            },
            "checkbox" : {
                "keep_selected_style": false,
                "whole_node": false
            },
        },
        "plugins" : [
        "contextmenu", "dnd", "search"
        ]
    });

    $("#jsTreeFilterInput").keyup(function () {

        var searchString = $(this).val();
        console.log(searchString);
        $('#jstree').jstree('search', searchString);
    });

    //$(function () {
    //    $("#plugins4").jstree({
    //        "plugins": ["search"]
    //    });
    //    var to = false;
    //    $('#plugins4_q').keyup(function () {
    //        if (to) { clearTimeout(to); }
    //        to = setTimeout(function () {
    //            var v = $('#plugins4_q').val();
    //            $('#plugins4').jstree(true).search(v);
    //        }, 250);
    //    });
    //});

    $('#jstree').on("select_node.jstree", function (e, data) {
        //alert(data.node.id.substring(0, 1).localeCompare("0"));
        if (data.node.id.substring(0, 1).localeCompare("0")) {
            
            $("#playlist").addClass("loading");

            currentFolderID = data.node.id;
            currentPlaylists[0].chunkToLoad = 1;
            populateMusicPlaylistByFolderId(currentFolderID);

            visiblePlaylist = 0;
            updatePlaylistSize(0);
            updatePlaylistDropdown();
        }
    });
    //JSTREE END

    $(function () {
        $("#slider").slider({
            orientation: "horizontal",
            min: 0,
            max: 100,
            range: "min",
            //disabled: true,
            value: 0,
            slide: function (event, ui) {
                musicplayer.currentTime = (ui.value / 100) * musicplayer.duration;
            }
        });
    });

    $(function () {
        $("#slider-vertical").slider({
            orientation: "horizontal",
            min: 0,
            max: 100,
            range: "min",
            value: 50,
            slide: function (event, ui) {
                musicplayer.volume = ui.value / 100;
                if (musicplayer.muted)
                    musicplayer.muted = false;
                switchToAudioPic();
            }
        });
        musicplayer.volume = $("#slider-vertical").slider("value") / 100;

        $("#muteBtn").click(function () {
            toggleSoundOnOf();
        });
    });

    $("#timelineBtn").click(function () {
        $("#slider-container").toggle();
    });

    $("#muteBtn-vertical").click(function () {
        toggleSoundOnOf();
    });

    function toggleSoundOnOf() {
        if (musicplayer.muted) {
            switchToAudioPic();
        }
        else {
            switchToMutedPic();
        }
    }

    function switchToPlayPic() {
        $('span:first', playAndPauseBtn).prop('class', '');
        $('span:first', playAndPauseBtn).prop('class', 'glyphicon glyphicon-play');
    }

    function switchToPausePic() {
        $('span:first', playAndPauseBtn).prop('class', '');
        $('span:first', playAndPauseBtn).prop('class', 'glyphicon glyphicon-pause');
    }

    function switchToMutedPic() {
        $('span:first', "#muteBtn").prop('class', '');
        $('span:first', "#muteBtn").prop('class', 'glyphicon glyphicon-volume-off');

        $('span:first', "#muteBtn-vertical").prop('class', '');
        $('span:first', "#muteBtn-vertical").prop('class', 'glyphicon glyphicon-volume-off');

        musicplayer.muted = true;
    }

    function switchToAudioPic() {
        $('span:first', "#muteBtn").prop('class', '');
        $('span:first', "#muteBtn-vertical").prop('class', '');
        $('span:first', "#muteBtn-vertical").prop('class', 'glyphicon glyphicon-volume-up');
        if (musicplayer.volume <= 0.5)
        {
            $('span:first', "#muteBtn").prop('class', 'glyphicon glyphicon-volume-down');
        }
        else
        {
            $('span:first', "#muteBtn").prop('class', 'glyphicon glyphicon-volume-up');
        }

        musicplayer.muted = false;
    }

    function stopSong() {
        musicplayer.pause();
        musicplayer.currentTime = 0;
        switchToPlayPic();
        removeCurrentlyPlaying();

        $("#currentlyPlayingSongSpan").html(" Playback stopped.");
        document.getElementById("playingSongAndTimeSeperator").style.display = "none";
        document.getElementById("timeDisplayCurrentlyPlayingInfo").style.display = "none";
    }

    function changeSongInActivePl(index, direction)
    {
        //alert(index);
        var activePl = currentPlaylists[activePlaylist];
        var indexToPlay = 0;

        if ((activePl.playPos == 0 && index == -1 && direction == -1) || (activePl.playPos == activePl.nrOfSongs - 1 && index == -1 && direction == 1))
            stopSong();
        else
        {
            if (index >= 0 && index <= activePl.nrOfSongs)
            {
                activePl.previousPlayPos = activePl.playPos;
                indexToPlay = index;
                activePl.playPos = index;
            }
            else if (index == -1)
            {
                activePl.previousPlayPos = activePl.playPos;
                indexToPlay = parseInt(activePl.playPos) + parseInt(direction);
                activePl.playPos = indexToPlay;
            }

            for (var i = 0; i < activePl.musicList.length; i++) {

                if (activePl.musicList[i].musicfiles.length > indexToPlay) {

                    musicSrc.src = activePl.musicList[i].musicfiles[indexToPlay].location;
                    currentlyPlayingSong = activePl.musicList[i].musicfiles[indexToPlay];
                    musicplayer.load();
                    musicplayer.play();

                    changeCurrentlyPlaying(activePl.previousPlayPos);
                    break;
                }
                else {
                    indexToPlay -= activePl.musicList[i].musicfiles.length;
                }
            }
        }
    }

    function removeAllClassesFromCurrenltyPlaying()
    {
        if ($('.playListFirstColumn').is('#currentlyPlaying'))
        {
            if ($("#currentlyPlaying").hasClass("glyphicon glyphicon-play"))
            {
                $("#currentlyPlaying").removeClass("glyphicon glyphicon-play");
            }

            if ($("#currentlyPlaying").hasClass("glyphicon glyphicon-pause"))
                $("#currentlyPlaying").removeClass("glyphicon glyphicon-pause");
        }
    }

    function removeCurrentlyPlaying()
    {
        if ($('.playListFirstColumn').is('#currentlyPlaying'))
        {
            removeAllClassesFromCurrenltyPlaying();

            $("#currentlyPlaying").attr("id", "playlistFirstColumnRow" + currentPlaylists[activePlaylist].playPos);
        }
    }

    function changeCurrentlyPlaying(previousAudioIndex)
    {
        //alert("CurrentAudioIndex: " + currentPlaylists[activePlaylist].playPos);
        //alert("PreviousAudioindex: " + previousAudioIndex);
        if ($('.playListFirstColumn').is('#currentlyPlaying'))
        {
            removeAllClassesFromCurrenltyPlaying();
            $("#currentlyPlaying").attr("id", "playlistFirstColumnRow" + currentPlaylists[activePlaylist].previousPlayPos);
            $("#playlistFirstColumnRow" + currentPlaylists[activePlaylist].playPos).attr("id", "currentlyPlaying");
            $("#currentlyPlaying").addClass("glyphicon glyphicon-play");
        }
        else
        {
            $("#playlistFirstColumnRow" + currentPlaylists[activePlaylist].playPos).attr("id", "currentlyPlaying");
            $("#currentlyPlaying").addClass("glyphicon glyphicon-play");
        }
    }

    function secToFormattedTime(sec)
    {
        var formattedTime = "";
        var hours = 0, min = 0, secLeft = 0;

        sec = Math.floor(sec);

        if (sec >= 3600)
        {
            hours = ~~(sec / 3600);
            sec -= 3600 * hours;
        }

        min = ~~(sec / 60);
        sec -= 60 * min;

        secLeft = sec;

        if (hours != 0)
            formattedTime = hours + ":";

        if (hours != 0 && min < 10)
            formattedTime += "0" + min + ":";
        else
            formattedTime += min + ":";

        if (secLeft < 10)
            formattedTime += "0" + secLeft;
        else
            formattedTime += secLeft;

        return formattedTime;
    }

    function timeUpdate() {
        var playPercent = 100 * (musicplayer.currentTime / musicplayer.duration);
        $("#slider").slider("option", "value", playPercent);

        var currentFormattedTime = secToFormattedTime(musicplayer.currentTime);
        //var durationFormattedTime = secToFormattedTime(duration);

        $(".timeDisplay p:first-of-type").html(currentFormattedTime + " / " + durationFormattedTime);

        if (musicplayer.ended)
        {
            //switchToPlayPic();
            changeSongInActivePl(-1, 1);
        }
    }

    function playUpdate()
    {
        if ($('.playListFirstColumn').is('#currentlyPlaying'))
        {
            if (musicplayer.paused) {
                if ($("#currentlyPlaying").hasClass("glyphicon glyphicon-play"))
                    $("#currentlyPlaying").removeClass("glyphicon glyphicon-play");

                $("#currentlyPlaying").addClass("glyphicon glyphicon-pause");
            }
            else {
                if ($("#currentlyPlaying").hasClass("glyphicon glyphicon-pause"))
                    $("#currentlyPlaying").removeClass("glyphicon glyphicon-pause");

                $("#currentlyPlaying").addClass("glyphicon glyphicon-play");
            }
        }
        else
        {
            $("#playlistFirstColumnRow" + currentPlaylists[activePlaylist].playPos).attr("id", "currentlyPlaying");
            $("#currentlyPlaying").addClass("glyphicon glyphicon-play");
        }
    }

    musicplayer.addEventListener("timeupdate", timeUpdate, false);

    //Gets audio file duration and waits until it canplay
    musicplayer.addEventListener("canplay", function () {
        durationFormattedTime = secToFormattedTime(musicplayer.duration);
    }, false);

    $("#stopBtn").click(function () {
        stopSong();
    });

    $(playAndPauseBtn).click(function () {
        if (musicplayer.paused) {
            musicplayer.play();
        }
        else {
            musicplayer.pause();
        }

        playUpdate();
    });

    musicplayer.onplay = function () {
        $("#currentlyPlayingSongSpan").html(currentlyPlayingSong.artist.name + " - " + currentlyPlayingSong.album + " - " + currentlyPlayingSong.title);
        document.getElementById("playingSongAndTimeSeperator").style.display = "block";
        document.getElementById("timeDisplayCurrentlyPlayingInfo").style.display = "block";
        switchToPausePic();
    };

    musicplayer.onpause = function () {
        switchToPlayPic();
    };

    var libraryViewJustBeenUpdated = false;

    $("#backwardBtn").click(function () {
        changeSongInActivePl(-1, -1);
    });

    $("#forwardBtn").click(function () {
        changeSongInActivePl(-1, 1);
    });

    //$playlist = $("#playlist");

    //$(document).on({
    //    ajaxStart: function () { $playlist.addClass("loading"); },
    //    ajaxStop: function () { $playlist.removeClass("loading"); }
    //});

});