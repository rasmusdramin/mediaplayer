﻿using MediaPlayer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace MediaPlayer.Controllers
{
    public class MusicController : Controller
    {
        MediaPlayerEntities db = new MediaPlayerEntities();

        // GET: Home
        [Authorize(Users = "Reaper")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Users = "Reaper")]
        public ActionResult Content()
        {
            return PartialView("content");
        }

        public JsonResult GetPlaylists()
        {
            List<PlayListSimple> userPlaylistSimples = new List<PlayListSimple>();

            using (var db = new MediaPlayerEntities())
            {
                List<playlistuser> userPlaylistuserEntries = db.siteuser.Find(User.Identity.Name).playlistuser.ToList<playlistuser>();

                foreach (playlistuser plaUsr in userPlaylistuserEntries)
                {
                    if ((bool)plaUsr.is_user_owner)
                    {
                        PlayListSimple currentPlaylist = new PlayListSimple();

                        currentPlaylist.id = plaUsr.playlist.id;
                        currentPlaylist.name = plaUsr.playlist.name;

                        userPlaylistSimples.Add(currentPlaylist);
                    }
                }
            }
            


            return Json(userPlaylistSimples);
        }

        public JsonResult GetMusicRootFolder()
        {
            JsTreeItem rootFolderItem = new JsTreeItem();

            rootFolderItem.parent = "#";
            rootFolderItem.id = "" + 1;
            rootFolderItem.text = "Music";
            rootFolderItem.state = new State();
            rootFolderItem.state.opened = false;
            rootFolderItem.children = true;

            return Json(rootFolderItem);
        }

        public JsonResult GetSubFoldersOfFolder(int id)
        {
            List<folder> foldersFromDBToReturn = new List<folder>();
            List<JsTreeItem> treeItems = new List<JsTreeItem>();

            using (var db = new MediaPlayerEntities())
            {
                List<folder> foldersFromDB = db.folder.ToList();
                List<musicfile> musicfilesFromDBForUser = db.Database.SqlQuery<musicfile>("select * from musicfile where id in (select musicfile_id from musicuser where siteuser_name = '" + User.Identity.Name + "')").ToList();
                foreach (folder currentFolder in foldersFromDB)
                {
                    if(currentFolder.parentfolder_id == id && ShouldFolderBeVisible(currentFolder, foldersFromDB, musicfilesFromDBForUser))
                    {
                        foldersFromDBToReturn.Add(currentFolder);
                    }
                }

                foreach (folder currentFolder in foldersFromDBToReturn)
                {
                    JsTreeItem newTreeItem = new JsTreeItem();

                    newTreeItem.parent = "" + currentFolder.parentfolder_id;
                    newTreeItem.id = "" + currentFolder.id;
                    newTreeItem.text = currentFolder.name;
                    newTreeItem.state = new State();
                    newTreeItem.state.opened = false;

                    List<folder> subFolders = foldersFromDB.Where(x => x.parentfolder_id == currentFolder.id).ToList();
                    bool anySubFolderVisible = false;

                    foreach(folder subFolder in subFolders)
                    {
                        if (ShouldFolderBeVisible(subFolder, subFolders, musicfilesFromDBForUser))
                            anySubFolderVisible = true;
                    }

                    if (anySubFolderVisible)
                    {
                        newTreeItem.children = true;
                    }
                    else
                        newTreeItem.children = false;

                    treeItems.Add(newTreeItem);
                }
            }

            return Json(treeItems);
        }

        public JsonResult PopulateJsTreeWithUserMusic()
        {
            List<musicfile> musicfilesForUserFromDB = db.siteuser.Find(User.Identity.Name).musicfile.ToList<musicfile>();
            List<folder> folderStructureFromDB = db.folder.ToList<folder>();

            List<JsTreeItem> jsTreeFolderNodes = new List<JsTreeItem>();
            List<JsTreeItem> jsTreeFileNodes = new List<JsTreeItem>();

            foreach (folder currentFolder in folderStructureFromDB)
            {
                if (ShouldFolderBeVisible(currentFolder, folderStructureFromDB, musicfilesForUserFromDB) || currentFolder.parentfolder_id == null)
                {
                    JsTreeItem newFolderJsTreeItem = new JsTreeItem();
                    newFolderJsTreeItem.state = new State();

                    if (currentFolder.parentfolder_id == null)
                    {
                        newFolderJsTreeItem.parent = "#";
                        newFolderJsTreeItem.state.opened = true;
                    }
                    else
                        newFolderJsTreeItem.parent = "" + currentFolder.parentfolder_id;

                    newFolderJsTreeItem.id = "" + currentFolder.id;
                    newFolderJsTreeItem.text = currentFolder.name;

                    jsTreeFolderNodes.Add(newFolderJsTreeItem);
                }
            }

            foreach (musicfile currentMusicfile in musicfilesForUserFromDB)
            {
                JsTreeItem newFileJsTreeItem = new JsTreeItem();
                newFileJsTreeItem.state = new State();

                newFileJsTreeItem.id = "0" + currentMusicfile.id;

                newFileJsTreeItem.parent = "" + currentMusicfile.folder_id;

                newFileJsTreeItem.text = currentMusicfile.filename;

                newFileJsTreeItem.icon = "glyphicon glyphicon-file";

                jsTreeFileNodes.Add(newFileJsTreeItem);
            }

            jsTreeFolderNodes.AddRange(jsTreeFileNodes);

            return Json(jsTreeFolderNodes);
        }

        public bool ShouldFolderBeVisible(folder folder, List<folder> allFolders, List<musicfile> userMusicfiles)
        {
            bool visible = false;

            if (userMusicfiles.Exists(x => x.folder_id == folder.id))
                visible = true;
            else if (allFolders.Exists(x => x.parentfolder_id == folder.id))
            {
                List<folder> subFolders = allFolders.FindAll(x => x.parentfolder_id == folder.id);
                foreach (folder subfolder in subFolders)
                {
                    if (ShouldFolderBeVisible(subfolder, subFolders, userMusicfiles))
                    {
                        visible = true;
                        break;
                    }
                }
            }

            return visible;
        }

        public JsonResult GetAlbumsByFolderId(int id, int chunkSize, int chunkNr)
        {
            List<AlbumSimple> albums = new List<AlbumSimple>();
            List<MusicFileSimple> musicfilesWithNoAlbum = new List<MusicFileSimple>();

            bool isThereMoreToLoad = true;

            if (id != 0)
            {
                int startIndexToLoad = (chunkSize * (chunkNr-1));
                string sqlQuery = "select * from musicfile where id in (select musicfileid from foldermusicfile where folderid = " + id + ") order by id offset " + startIndexToLoad + " rows fetch next " + chunkSize + " rows only";

                //List<musicfile> musicfilesForUserFromDB = GetAllMusicfilesInFolderAndSubFolders(id);
                List<musicfile> musicfilesForUserFromDB = db.Database.SqlQuery<musicfile>(sqlQuery).ToList();
                //System.Diagnostics.Debug.WriteLine("Main call: " + musicfilesForUserFromDB.Count);

                if (musicfilesForUserFromDB.Count < chunkSize)
                    isThereMoreToLoad = false;

                List<album> dbAlbums = db.album.ToList<album>();

                foreach (album albumInDb in dbAlbums)
                {
                    if (musicfilesForUserFromDB.Exists(x => x.album_id == albumInDb.id))
                    {
                        AlbumSimple newAlbum = new AlbumSimple();
                        newAlbum.musicfiles = new System.Collections.ObjectModel.Collection<MusicFileSimple>();

                        newAlbum.id = albumInDb.id;
                        newAlbum.name = albumInDb.name;

                        foreach (artist artistInDb in albumInDb.artist.ToList<artist>())
                        {
                            ArtistSimple newArtist = new ArtistSimple();
                            newAlbum.artists = new System.Collections.ObjectModel.Collection<ArtistSimple>();

                            newArtist.id = artistInDb.id;
                            newArtist.name = artistInDb.name;
                            newArtist.description = artistInDb.description;

                            newAlbum.artists.Add(newArtist);
                        }

                        int highestYear = 0, lowestYear = 9999;

                        foreach (albumyear albumyearInDb in albumInDb.albumyear.ToList<albumyear>())
                        {
                            if (albumyearInDb.year < lowestYear)
                                lowestYear = albumyearInDb.year;
                            else if (albumyearInDb.year > highestYear)
                                highestYear = albumyearInDb.year;
                        }

                        newAlbum.years = new System.Collections.ObjectModel.Collection<int>();

                        if (highestYear == lowestYear)
                            newAlbum.years.Add(highestYear);
                        else
                        {
                            newAlbum.years.Add(lowestYear);
                            newAlbum.years.Add(highestYear);
                        }

                        albums.Add(newAlbum);
                    }
                }

                string musicSourceUrl = db.source.Find(1).url;

                foreach (musicfile musicfileInDB in musicfilesForUserFromDB)
                {
                    //Complete musicfileInDB
                    musicfileInDB.folder = db.folder.Find(musicfileInDB.folder_id);
                    musicfileInDB.genre = db.genre.Find(musicfileInDB.genre_id);
                    musicfileInDB.artist = db.Database.SqlQuery<artist>("select * from artist where id in (select artist_id from musicfileartist where musicfile_id = " + musicfileInDB.id + ")").ToList();
                    musicfileInDB.album = db.album.Find(musicfileInDB.album_id);
                    //END of complete

                    MusicFileSimple newMusicfile = new MusicFileSimple();

                    newMusicfile.id = musicfileInDB.id;
                    newMusicfile.filename = musicfileInDB.filename;
                    newMusicfile.location = musicSourceUrl + musicfileInDB.folder.folderpath + "/" + musicfileInDB.filename;
                    newMusicfile.audioformat = musicfileInDB.audioformat;
                    newMusicfile.lenght = musicfileInDB.lenght;
                    newMusicfile.channels = musicfileInDB.channels;
                    newMusicfile.bitrate = musicfileInDB.bitrate;
                    newMusicfile.title = musicfileInDB.title;
                    newMusicfile.tracknr = musicfileInDB.tracknr;
                    //fix if genre or artist is null from DB
                    if (musicfileInDB.genre != null)
                    {
                        newMusicfile.genre = new GenreSimple();
                        newMusicfile.genre.id = musicfileInDB.genre.id;
                        newMusicfile.genre.name = musicfileInDB.genre.name;
                    }
                    else
                        newMusicfile.genre = null;

                    if (musicfileInDB.artist.Count > 0)
                    {
                        newMusicfile.artist = new ArtistSimple();
                        newMusicfile.artist.id = musicfileInDB.artist.ElementAt(0).id;
                        newMusicfile.artist.name = musicfileInDB.artist.ElementAt(0).name;
                    }
                    else
                        newMusicfile.artist = null;

                    newMusicfile.songyear = musicfileInDB.songyear;
                    newMusicfile.composer = musicfileInDB.composer;
                    newMusicfile.performer = musicfileInDB.performer;
                    newMusicfile.author = musicfileInDB.author;
                    newMusicfile.album = "???";

                    if (albums.Exists(x => x.id == musicfileInDB.album_id))
                    {
                        newMusicfile.album = albums.Find(x => x.id == musicfileInDB.album_id).name;
                        albums.Find(x => x.id == musicfileInDB.album_id).musicfiles.Add(newMusicfile);
                    }
                    else
                        musicfilesWithNoAlbum.Add(newMusicfile);
                }
            }

            if (musicfilesWithNoAlbum.Count != 0)
            {
                AlbumSimple albumForAlbumlessMusicfiles = new AlbumSimple();

                albumForAlbumlessMusicfiles.id = 0;
                albumForAlbumlessMusicfiles.name = "No Album Specified";

                albumForAlbumlessMusicfiles.years = new System.Collections.ObjectModel.Collection<int>();
                albumForAlbumlessMusicfiles.artists = new System.Collections.ObjectModel.Collection<ArtistSimple>();
                albumForAlbumlessMusicfiles.musicfiles = new System.Collections.ObjectModel.Collection<MusicFileSimple>();

                albumForAlbumlessMusicfiles.years.Add(0);

                ArtistSimple tempArtist = new ArtistSimple();
                tempArtist.id = 0;
                tempArtist.name = "???";
                albumForAlbumlessMusicfiles.artists.Add(tempArtist);

                foreach (MusicFileSimple albumlessMusicfile in musicfilesWithNoAlbum)
                {
                    albumForAlbumlessMusicfiles.musicfiles.Add(albumlessMusicfile);
                }

                albums.Add(albumForAlbumlessMusicfiles);
            }

            if (!isThereMoreToLoad)
                albums.Add(null);

            return Json(albums);
        }

        public List<musicfile> GetAllMusicfilesInFolderAndSubFolders(int folderId)
        {
            List<musicfile> musicfiles = new List<musicfile>();

            using (var db = new MediaPlayerEntities())
            {
                //musicfiles = siteuserFromDB.ElementAt(0).musicfile.Where(x => x.folder_id == folderId).ToList();
                //db.siteuser.First(u => u.username == User.Identity.Name).Include(u => u.musicfile).Where(x => x.folder_id == folderId).ToList();
                //speedup, slow as hell.
                
                musicfiles = db.Database.SqlQuery<musicfile>("select * from musicfile where id in (select musicfile_id from musicuser where siteuser_name = '" + User.Identity.Name + "') and folder_id = " + folderId).ToList();
                foreach(musicfile currentMusicfile in musicfiles)
                {
                    //if (musicfiles.Count > 50)
                    //    break;

                    currentMusicfile.folder = db.folder.Find(currentMusicfile.folder_id);
                    currentMusicfile.genre = db.genre.Find(currentMusicfile.genre_id);
                    currentMusicfile.artist = db.Database.SqlQuery<artist>("select * from artist where id in (select artist_id from musicfileartist where musicfile_id = " + currentMusicfile.id + ")").ToList();
                }

                List<folder> subFolders = db.folder.Where(x => x.parentfolder_id == folderId).ToList<folder>();
                if (db.folder.AsNoTracking().ToList<folder>().Exists(x => x.parentfolder_id == folderId))
                {
                    foreach (folder subfolder in subFolders)
                    {
                        //if (musicfiles.Count > 50)
                        //    break;

                        musicfiles.AddRange(GetAllMusicfilesInFolderAndSubFolders(subfolder.id));
                    }
                }
            }

            //System.Diagnostics.Debug.WriteLine("Per call: " + musicfiles.Count);
            return musicfiles;
        }

        public JsonResult CreatePlaylist(string playlistName)
        {
            Trace.WriteLine("PLAYLSITNAME:->  " + playlistName);
            List<playlist> allPlaylistInDB = db.playlist.ToList<playlist>();
            var newPlaylist = db.playlist.Create();
            var newPlaylistuser = db.playlistuser.Create();

            if (allPlaylistInDB.Count == 0)
                newPlaylist.id = 1;
            else
                newPlaylist.id = allPlaylistInDB.ElementAt(allPlaylistInDB.Count - 1).id + 1;

            newPlaylist.name = playlistName;

            newPlaylistuser.playlist_id = newPlaylist.id;

            var pageUser = db.siteuser.Find(User.Identity.Name);

            newPlaylistuser.siteuser_name = pageUser.username;
            newPlaylistuser.is_user_owner = true;

            db.playlist.Add(newPlaylist);
            db.playlistuser.Add(newPlaylistuser);
            pageUser.playlistuser.Add(newPlaylistuser);
            newPlaylist.playlistuser.Add(newPlaylistuser);

            try
            {
                db.SaveChanges();
            }
            catch (Exception dbEx)
            {
                while (dbEx.InnerException != null) dbEx = dbEx.InnerException;
                Trace.WriteLine(dbEx.Message);

                //foreach (var validationErrors in dbEx.EntityValidationErrors)
                //{
                //    foreach (var validationError in validationErrors.ValidationErrors)
                //    {
                //        Trace.TraceInformation("Property: {0} Error: {1}",
                //                                validationError.PropertyName,
                //                                validationError.ErrorMessage);
                //    }
                //}
            }

            return Json(newPlaylist.id);
        }
    }
}