﻿using MediaPlayer.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MediaPlayer.Controllers
{
    public class HomeController : Controller
    {
        MediaPlayerEntities db = new MediaPlayerEntities();

        [HttpGet]
        public ActionResult Index()
        {
            string currentDirectory = "C:\\GenkaiWebSiteData\\IPLog.txt";
            //string currentDirectory = System.Web.HttpContext.Current.Server.MapPath("~/") + "\\IPLog.txt";
            // This text is added only once to the file.
            //if (!System.IO.File.Exists(path))
            //{
            // Create a file to write to.

            try
            {
                using (StreamWriter sw = System.IO.File.AppendText(currentDirectory))
                {
                    string ip = Request.UserHostAddress;
                    sw.WriteLine(ip);
                }
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e.ToString());
                ViewBag.exception = e.ToString();
            }
            
            //}

            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Music");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Index(MediaPlayer.Models.UserModel user)
        {
            if(ModelState.IsValid)
            {
                if(IsValid(user.username, user.password))
                {
                    FormsAuthentication.SetAuthCookie(user.username, false);
                    return RedirectToAction("Index", "Music");
                }
                else
                {
                    ModelState.AddModelError("", "Username or Password is incorrect.");
                }
            }

            return View(user);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        private bool IsValid(string username, string password)
        {
            bool isValid = false;
            var crypto = new SimpleCrypto.PBKDF2();

            //try
            //{
                var user = db.siteuser.FirstOrDefault(u => u.username == username);
            //}
            //catch(Exception e)
            //{
            //    System.Console.Write(e.StackTrace);
            //}

            if (user != null)
            {
                if (user.password == crypto.Compute(password, user.passwordsalt))
                {
                    isValid = true;
                }
            }

            return isValid;
        }
    }
}