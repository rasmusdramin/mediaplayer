﻿using System.Web;
using System.Web.Optimization;

namespace test
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Bundles/Scripts/sitejs").Include(
          "~/Scripts/layout.js",
          "~/Scripts/split-pane.js",
          "~/Scripts/MusicPlayer.js"));

            bundles.Add(new StyleBundle("~/Bundles/Style/sitecss").Include(
                "~/Style/jquery-ui.min.css",
                "~/Style/jquery-ui.structure.min.css",
                "~/Style/split-pane.css",
                "~/Style/layout.css",
                "~/Style/musiccontentview.css",
                "~/Style/playlist.css",
                "~/Style/musicplayer.css"));
        }
    }
}
